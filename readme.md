# gradle-execution-reporter-plugin

This plugin monitors the Gradle's tasks and write the status in a json file (`.execution-reporter-plugin/tasks.json` by default)

## Settings
You can customize the plugin by using gradle's DSL in the `build.gradle` / `build.gradle.kts`

```kotlin
reporterplugin {
    folderName = "superFolder"
    fileBaseName = "fileName"
    report = ca.watier.gradle.plugins.ReportTaskStatus.ANY
}
```

### folderName
The name of the folder that the file will be inserted.

### fileBaseName
The name of the file, don’t provide the extension in the name.

### report
This parameter filter the tasks, and add them to the file if they have a matching status; depending on if they FAILED / PASSED or ANY status.

## Example of file
* name : The name of the failed task
* exception : The exception that occurred, if present.

```json
[
  {
    "name": "compileJava",
    "exception": "org.gradle.api.internal.tasks.compile.CompilationFailedException"
  }
]
```

In this case, we know that the compiler was not able to compile the code.