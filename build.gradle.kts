import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val pluginVersion = "1.3.0-SNAPSHOT"

plugins {
    kotlin("jvm") version "1.5.0"
    id("com.gradle.plugin-publish") version "0.14.0"
    id("java-gradle-plugin")
    id("maven-publish")
    application
}

group = "ca.watier.gradle.plugins"
version = pluginVersion

repositories {
    mavenLocal()
    mavenCentral()
    gradlePluginPortal()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

gradlePlugin {
    plugins {
        create("executionReporterPlugin") {
            id = "ca.watier.gradle.plugins.gradle-execution-reporter-plugin"
            implementationClass = "ca.watier.gradle.plugins.ExecutionReporterPlugin"
        }
    }
}

pluginBundle {
    website = "https://gitlab.com/doi9t/gradle-execution-reporter-plugin"
    vcsUrl = "https://gitlab.com/doi9t/gradle-execution-reporter-plugin"

    (plugins) {
        "executionReporterPlugin" {
            displayName = "Gradle Execution Reporter Plugin"
            tags = listOf("report", "json", "output", "tasks")
            version = pluginVersion
            description = "This plugin monitors the Gradle's tasks and write the result in a json file."
        }
    }

    mavenCoordinates {
        groupId = "ca.watier.gradle.plugins"
        artifactId = "gradle-execution-reporter-plugin"
        version = pluginVersion
    }
}

dependencies {
    implementation("com.google.code.gson:gson:2.8.6")
}
