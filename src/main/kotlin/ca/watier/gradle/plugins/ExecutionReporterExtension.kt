/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.gradle.plugins

import ca.watier.gradle.plugins.models.OptionTaskStatus
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional

abstract class ExecutionReporterExtension {
    @Input
    @Optional
    var folderName: String = ".execution-reporter-plugin"

    @Input
    @Optional
    var fileBaseName: String = "tasks"

    @Input
    @Optional
    var option: OptionTaskStatus = OptionTaskStatus.FAILED
}