/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.gradle.plugins

import ca.watier.gradle.plugins.models.OptionTaskStatus
import ca.watier.gradle.plugins.models.TaskModel
import ca.watier.gradle.plugins.models.TaskStatus
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.gradle.BuildListener
import org.gradle.BuildResult
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.execution.TaskExecutionListener
import org.gradle.api.initialization.Settings
import org.gradle.api.internal.tasks.TaskStateInternal
import org.gradle.api.invocation.Gradle
import org.gradle.api.tasks.TaskState
import java.io.File

@Suppress("unused")
open class ExecutionReporterPlugin : Plugin<Project> {
    var taskStatusFile: File? = null;
    var gsonBuilder: GsonBuilder = GsonBuilder().serializeNulls();
    var gson: Gson = gsonBuilder.create();

    override fun apply(project: Project) {
        val pluginExtension: ExecutionReporterExtension = createProjectExtension(project);
        val taskStatusFileContent: MutableList<TaskModel> = mutableListOf();

        val gradle: Gradle = project.gradle;

        gradle.addListener(object : TaskExecutionListener {
            override fun beforeExecute(task: Task) {}

            override fun afterExecute(task: Task, state: TaskState) {
                if (state is TaskStateInternal) {
                    val option: OptionTaskStatus = pluginExtension.option;

                    val failure = state.failure;
                    val isCurrentTaskFailed: Boolean = failure != null;

                    if (isCurrentTaskFailed && option == OptionTaskStatus.PASSED) {
                        return;
                    } else if (!isCurrentTaskFailed && option == OptionTaskStatus.FAILED) {
                        return;
                    }

                    val taskModel = TaskModel();
                    taskModel.name = task.name;

                    if (isCurrentTaskFailed) { // Failed
                        val cause = failure?.cause;

                        if (cause != null) {
                            taskModel.cause = cause.javaClass.name;
                        }
                    } else {
                        taskModel.status = TaskStatus.PASSED;
                    }

                    taskStatusFileContent.add(taskModel);
                }
            }
        })

        gradle.addBuildListener(object : BuildListener {
            override fun buildStarted(gradle: Gradle) {}

            override fun settingsEvaluated(settings: Settings) {}

            override fun projectsLoaded(gradle: Gradle) {}

            override fun projectsEvaluated(gradle: Gradle) {
                taskStatusFile = createFailedTaskJsonFile(project, pluginExtension);
            }

            override fun buildFinished(result: BuildResult) {
                taskStatusFile?.writeText(gson.toJson(taskStatusFileContent));
            }
        });
    }

    private fun createFailedTaskJsonFile(project: Project, pluginExtension: ExecutionReporterExtension): File {
        val folderName = pluginExtension.folderName;    //TODO: Sanitize the path ?
        val fileBaseName = pluginExtension.fileBaseName; //TODO: Sanitize the file name ?

        val taskStatusFile: File = project.file("$folderName${File.separatorChar}$fileBaseName.json");
        val pluginFolder = taskStatusFile.parentFile;

        if (!pluginFolder.exists()) {
            pluginFolder.mkdir();
        }

        taskStatusFile.createNewFile();

        return taskStatusFile;
    }

    private fun createProjectExtension(project: Project): ExecutionReporterExtension {
        return project.extensions.create("reporterplugin", ExecutionReporterExtension::class.java);
    }
}